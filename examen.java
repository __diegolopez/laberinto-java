import java.util.Scanner;

public class examen{
    public static void main (String[] args){

        Scanner leer = new Scanner(System.in);

        //Se declaran las variables a utilizar
        int opcmenu, factor1, op1dificultad, res1, res2, res3, res4, res5; 
        int factor21, factor22, factor23, factor24, factor25;
        double puntuacion;
        int pregunta1, pregunta2, pregunta3, pregunta4, pregunta5; 
        factor1 = 0;
        factor21 = 0;
        factor22 = 0;
        factor23 = 0;
        factor24 = 0;
        factor25 = 0;
        puntuacion = 0;
        res1 = 0;
        res2 = 0;
        res3 = 0;
        res4 = 0;
        res5 = 0;

        System.out.println("====================================");
        System.out.println(" EXAMEN AUTOMATICO");
        System.out.println("====================================");
        

        do{
                //Seleccion del menu principal
            System.out.println("1. Configuracion del sistema");
            System.out.println("2. Realizacion del examen");
            System.out.println("3. Salir del sistema");
            System.out.println(" ");
            System.out.print("Elija su opcion: ");
            opcmenu = leer.nextInt();
            System.out.println(" ");
            System.out.println(" ");

            //Menu de la opcion 1
            if ( opcmenu == 1){
                System.out.println("CONFIGURACION");
                System.out.println("-------------------------------");
                System.out.print("Seleccione el factor a evaluar: ");
                factor1 = leer.nextInt();

                //Usa las condicionales para que el dato este dentro del rango
                if( (factor1 <= 10) && (factor1 >= 2)){  
                    System.out.println(" ");
                    System.out.println("Nivel de dificultad: ");
                    System.out.println("1. Facil");
                    System.out.println("2. Normal");
                    System.out.println("3. Avanzado");
                    System.out.println(" ");
                    System.out.print("Elija el nivel de dificultad: ");
                    op1dificultad = leer.nextInt();
                    if( op1dificultad == 1){    
                        factor21 = factor21 + ((int)(Math.random()*6+1));
                        factor22 = factor22 + ((int)(Math.random()*6+1));
                        factor23 = factor23 + ((int)(Math.random()*6+1));
                        factor24 = factor24 + ((int)(Math.random()*6+1));
                        factor25 = factor25 + ((int)(Math.random()*6+1));
                    }else if (op1dificultad == 2){
                        factor21 = factor21 + ((int)(Math.random()*10+1));
                        factor22 = factor22 + ((int)(Math.random()*10+1));
                        factor23 = factor23 + ((int)(Math.random()*10+1));
                        factor24 = factor24 + ((int)(Math.random()*10+1));
                        factor25 = factor25 + ((int)(Math.random()*10+1));
                    }else if( op1dificultad == 3){
                        factor21 = factor21 + ((int)(Math.random()*10+1));
                        factor22 = factor22 + ((int)(Math.random()*10+1));
                        factor23 = factor23 + ((int)(Math.random()*10+1));
                        factor24 = factor24 + ((int)(Math.random()*10+1));
                        factor25 = factor25 + ((int)(Math.random()*10+1));    
                    }else{
                        System.out.println("Usted no ha seleccionado una de las opciones disponibles");
                    }
                    
                }else {
                    System.out.println("Usted no ha ingresado un numero en el rango permitido");
                    System.out.println(" ");
                }

                //Se configura el menu 2
            }else if ( opcmenu == 2){
                System.out.println("EXAMEN");
                System.out.println("----------------------------------");
                pregunta1 = factor1 * factor21;
                pregunta2 = factor1 * factor22;
                pregunta3 = factor1 * factor23; 
                pregunta4 = factor1 * factor22;
                pregunta5 = factor1 * factor23;

                //Se realizan las preguntas
                System.out.print("Cuanto es "+factor1+" X "+factor21+" = " ); res1 = leer.nextInt();
                System.out.print("Cuanto es "+factor1+" X "+factor22+" = " ); res2 = leer.nextInt();
                System.out.print("Cuanto es "+factor1+" X "+factor23+" = " ); res3 = leer.nextInt();
                System.out.print("Cuanto es "+factor1+" X "+factor24+" = " ); res4 = leer.nextInt();
                System.out.print("Cuanto es "+factor1+" X "+factor25+" = " ); res5 = leer.nextInt();
             
                //Se califican  las preguntas
                if( res1 == pregunta1){
                    puntuacion = puntuacion + 2;
                }else if( res1 != pregunta1){
                    puntuacion = puntuacion - 0.5;
                }
                if( res2 == pregunta2){
                    puntuacion = puntuacion + 2;
                }else if( res2 != pregunta2){
                    puntuacion = puntuacion - 0.5;
                }
                if( res3 == pregunta3){
                    puntuacion = puntuacion + 2;
                }else if( res3 != pregunta3){
                    puntuacion = puntuacion - 0.5;
                }
                if( res4 == pregunta4){
                    puntuacion = puntuacion + 2;
                }else if( res4 != pregunta4){
                    puntuacion = puntuacion - 0.5;
                }
                if( res5 == pregunta5){
                    puntuacion = puntuacion + 2;
                }else if( res5 != pregunta5){
                    puntuacion = puntuacion - 0.5;
                }


                //Revisa si el usuario gano o perdio
                System.out.println(" ");

                System.out.println(" ");
                if(puntuacion >= 5){
                    System.out.println("El usuario a aprobado con una nota de "+puntuacion);
                }else if ( puntuacion < 5){
                    System.out.println("El usuario a reprobado con una nota de "+puntuacion);
                }
                System.out.println(" ");

            }else if ( opcmenu == 3){
                System.out.println("====================================");
                System.out.println("Saliendo del programa");

            }
        }while(opcmenu != 3);
       
    }
}