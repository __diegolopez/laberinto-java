import java.util.Scanner;
import java.util.Random;

public class juego {
    public static void main (String [] args){
        Scanner entrada = new Scanner(System.in);
        Random dado = new Random();

        int jugador1, jugador2, jugador3, jugador4;
        int numero, j, elGanador, jugadorUnoCarcel, jugadorDosCarcel, jugadorTresCarcel, jugadorCuatroCarcel;
        String laPausa;
        jugador1 = 0;
        jugador2 = 0;  
        jugador3 = 0;
        jugador4 = 0;
        jugadorUnoCarcel = 0;
        jugadorDosCarcel = 0;
        jugadorTresCarcel = 0;
        jugadorCuatroCarcel = 0;
        elGanador = 0;

        System.out.println("Bienvenido al juego de la OCA");
        
         do{

            System.out.println("Pulse una tecla para continuar");
            laPausa = entrada.nextLine();

            // Condiciones
            if ( (jugador1 == 30) ){
                jugador1 = jugador1 - 5;
             }else if ( (jugador2 == 30) ){
                 jugador2 = jugador2 - 5;
             }else if ( (jugador3 == 30) ){
                 jugador3 = jugador3 - 5;
             }else if ( (jugador4 == 30) ){
                 jugador4 = jugador4 - 5;
             }

             if ( (jugador1 == 31) ){
                jugador1 = jugador1 + 5;
             }else if ( (jugador2 == 31) ){
                 jugador2 = jugador2 + 5;
             }else if ( (jugador3 == 31) ){
                 jugador3 = jugador3 + 5;
             }else if ( (jugador4 == 31) ){
                 jugador4 = jugador4 + 5;
             } 

             if ( (jugador1 == 58) ){
                jugador1 = 0;
             }else if ( (jugador2 == 58) ){
                 jugador2 = 0;
             }else if ( (jugador3 == 58) ){
                 jugador3 = 0;
             }else if ( (jugador4 == 58) ){
                 jugador4 = 0;
             }

             if ( jugador1 == 23){
                 jugadorUnoCarcel = 3;
             }else if ( jugador2 == 23){
                jugadorDosCarcel = 3;
            }else if ( jugador3 == 23){
                jugadorTresCarcel = 3;
            }else if ( jugador4 == 23){
                jugadorCuatroCarcel = 3;
            }

            if (jugador1 > 63) {
                jugador1 = 63 - (jugador1 - 63);
            }else if (jugador2 > 63){
                jugador2 = 63 - (jugador2 - 63);
            }else if (jugador3 > 63){
                jugador3 = 63 - (jugador3 - 63);
            }else if (jugador4 > 63){
                jugador4 = 63 - (jugador4 - 63);
            }

             //Jugado 1

             if ( jugadorUnoCarcel == 0){
             System.out.println("*************************************************************************");
             System.out.println(" ");
             System.out.println("Es el turno del jugador 1");
             numero = (dado.nextInt(6)+1);
             System.out.println("El valor del dado es " +numero);
             jugador1 = jugador1 + numero;
             if ( jugador1 >= 63 && elGanador == 0){
                 elGanador = 1;
             }
             }else {
                 jugadorUnoCarcel = jugadorUnoCarcel - 1;
             }
             
             for (j=1;j<jugador1;j=j+1){
                System.out.print("-");
            }

            // Jugador 2
            if ( jugadorDosCarcel == 0){
                System.out.println(" ");
             System.out.println("Es el turno del jugador 2");
             numero = (dado.nextInt(6)+1);
             System.out.println("El valor del dado es " +numero);
             jugador2 = jugador2 + numero;
             if ( jugador2 >= 63 && elGanador == 0 ){
                 elGanador = 2;
             }
            }else{
                jugadorDosCarcel = jugadorDosCarcel - 1;
            }
             
             for (j=1;j<jugador2;j=j+1){
                System.out.print("-");
            }


            //Jugador 3
            if (jugadorTresCarcel == 0){
             System.out.println(" ");
             System.out.println("Es el turno del jugador 3");
             numero = (dado.nextInt(6)+1);
             System.out.println("El valor del dado es " +numero);
             jugador3 = jugador3 + numero;
             if ( jugador3 >= 63 && elGanador == 0){
                elGanador = 3;
            }
            }else{
                jugadorTresCarcel = jugadorTresCarcel - 1;
            }
             
             for (j=1;j<jugador3;j=j+1){
                System.out.print("-");
            }


            //Jugador 4
            if(jugadorCuatroCarcel == 0){
             System.out.println(" ");
             System.out.println("Es el turno del jugador 4");
             numero = (dado.nextInt(6)+1);
             System.out.println("El valor del dado es " +numero);
             jugador4 = jugador4 + numero;
             if ( jugador4 >= 63 && elGanador == 0){
                elGanador = 4;
            }
            }else{
                jugadorCuatroCarcel = jugadorCuatroCarcel - 1;
            }
             
             for (j=1;j<jugador4;j=j+1){
                System.out.print("-");
            }
            System.out.println(" ");
            System.out.println("Jugador1["+jugador1+"] Jugador2["+jugador2+"] Jugador3["+jugador3+"] Jugador4["+jugador4+"]");
            


            
             
         }while ( elGanador == 0 );

         System.out.println("El ganador es el jugador " +elGanador);
    }
}