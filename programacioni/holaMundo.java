public class holaMundo {

    static factorDeCorreccion = 1;          //Esto es una variable global, esta variable la puedo utilizar en cualquier método o función del programa.
    public static void main(String [] args){
        
        int a, b, suma, resta, multiplicacion, division;
        a = 5;
        b = 10;

        suma = suma(a, b);
        resta = resta(a, b);

        System.out.println("El resultado de la suma es: "+suma);
        System.out.println("El resultado de la resta es: "+resta);
    }

    public static int suma(int sumando1, int sumando2){     //Este es un método, se declaran las variables dentro de los parentesis
        
        int resultado;
        resultado = sumando1 + sumando2;
        return resultado;
    }

    public static int resta(int operador1, int operador2){
        int resultado;
        resultado = operador2 - operador1;
        return resultado;
    }

}