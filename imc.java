import java.util.Scanner;

public class imc{

    public static void main (String[] args){

        Scanner leer = new Scanner(System.in);

        double talla, peso, res, descriptor;

        System.out.print("Ingrese la talla del paciente (en metros) ");
        talla = leer.nextDouble();
        System.out.print("Ingrese el peso del paciente (en kg) ");
        peso = leer.nextDouble();

        res = (peso / (talla * talla));

        if (res < 18.5){
            System.out.println("Usted posee un peso insuficiente, su IMC es "+res);
        }else if ((res >= 18.5) && (res < 24.9)){
            System.out.println("Usted posee un normopeso, su IMC es "+res);
        }else if ((res >= 25) && (res < 26.9)){
            System.out.println("Usted posee sobrepeso de grado I , su IMC es "+res);
        }else if ((res >= 27) && (res < 29.9)){
            System.out.println("Usted posee sobrepeso de grado II, su IMC es "+res);
        }else if ((res >= 30) && (res < 34.9)){
            System.out.println("Usted posee Obesidad de tipo I , su IMC es "+res);
        }else if ((res >= 35) && (res < 39.9)){
            System.out.println("Usted posee Obesidad de tipo II, su IMC es "+res);
        }else if ((res >= 40) && (res < 49.9)){
            System.out.println("Usted posee Obesidad de tipo III, su IMC es "+res);
        }else if (res > 50){
            System.out.println("Usted posee Obesidad de tipo IV, su IMC es "+res);
        }else {
            System.out.println("Ha ocurrido un error en el calculo del IMC");
        }


        if (res < 18.5){
            descriptor = "Peso insucifiente";
        }else if ((res >= 18.5) && (res < 24.9)){
            descriptor = "Normopeso";
        }else if ((res >= 25) && (res < 26.9)){
            descriptor = "Sobrepeso de grado I";
        }else if ((res >= 27) && (res < 29.9)){
            descriptor = "Sobre peso de grado II";
        }else if ((res >= 30) && (res < 34.9)){
            descriptor = "Obesidad de grado I";
        }else if ((res >= 35) && (res < 39.9)){
            descriptor = "Obesidad de grado II";
        }else if ((res >= 40) && (res < 49.9)){
            descriptor = "Obesidad de grado III";
        }else if (res > 50){
            descriptor = "Obesidad de grado IV";
        }else {
            descriptor = "Ha ocurrido un error en el calculo";
        }
        

    }
}