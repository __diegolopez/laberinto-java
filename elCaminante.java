import java.util.Scanner;

public class elCaminante{
    public static void main(String[] args){
        Scanner entrada = new Scanner(System.in);

        int miFila, miColumna, salir;
        int personajeFila, personajeColumna;
        String ordenUsuario;
        personajeFila = 2;
        personajeColumna = 1;
        miFila = 0;
        miColumna = 0;
        salir = 0;

        String [][] elMundo ={
            {"Mar",     "Monte     ",  "Monte     ",    "Monte     ",   "Monte     ",   "Monte      ",  "Monte"},
            {"Mar",     "Bosque    ",  "Bosque    ",    "Colina    ",   "Ruinas    ",   "Bosque     ",  "Monte"},
            {"Mar",     "Casa      ",  "Bosque    ",    "Arbol Raro",   "Lago      ",   "Lago       ",  "Monte"}, 
            {"Mar",     "Planicie  ",  "Bosque    ",    "Pozo      ",   "Lago      ",   "Lago       ",  "Cueva"}, 
            {"Mar",     "Bosque    ",  "Bosque    ",    "Bosque    ",   "Templo    ",   "Bosque     ",  "Monte"}, 
            {"Mar",     "Precipicio",  "Precipicio",    "Precipicio",   "Precipicio",   "Precipicio ",  "Monte"} 
        };

        do{

        System.out.println(" ");
        System.out.println("Bienvenido al Mundo de Maria, empiezas en la casa y tienes que salir... jeje");
        System.out.println(" ");

        // Inicio de la impresión del mapa
        for(miFila=0; miFila<elMundo.length; miFila= miFila + 1){
            System.out.print(" | ");
            for(miColumna=0; miColumna<elMundo[miFila].length; miColumna=miColumna+1){
                
                System.out.print(elMundo[miFila][miColumna] );

                if ((personajeFila==miFila) && (personajeColumna == miColumna)){
                    System.out.print("*| ");
                }else {
                    System.out.print(" | ");
                }
            }

            System.out.println();
        }

        //Empieza a preguntar al personaje que desea hacer
        System.out.println("Ahora decide hacia donde te quieres mover"); //Usar la primera letra mayuscula
        ordenUsuario = entrada.nextLine();

        if(ordenUsuario.equals("Norte") && (personajeFila>0)){
            personajeFila = personajeFila - 1;
        }
        if(ordenUsuario.equals("Sur") && (personajeColumna<elMundo.length-1)){
            personajeFila = personajeFila + 1;
        }
        if(ordenUsuario.equals("Este")  && (personajeColumna<elMundo[personajeFila].length-1)){
            personajeColumna = personajeColumna + 1;
        }
        if(ordenUsuario.equals("Oeste") && (personajeColumna>0)){
            personajeColumna = personajeColumna - 1;
        }
        if(ordenUsuario.equals("Salir")){

            salir = 1;
        }

        System.out.println("Fila = "+personajeFila+ " Columna = "+personajeColumna);

        System.out.println("==========================================================================================");

        }while(salir != 1);

    }
}