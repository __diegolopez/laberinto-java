import java.util.Scanner;

public class ejemploBucleWhile{
    public static void main (String[] args){

        Scanner leer = new Scanner(System.in);

        int variableDeControl;
        int numeroDeRepeticiones;
        variableDeControl = 0;
        numeroDeRepeticiones = leer.nextInt();

        System.out.println("Inicio de la estructura de control");
        while( variableDeControl < numeroDeRepeticiones ){
            System.out.println(" variableDeControl = " +variableDeControl);
            variableDeControl = variableDeControl + 1;
        }
        System.out.println("Fin de la estructura de control");


    }
}