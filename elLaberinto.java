import java.util.Scanner;
public class elLaberinto{
	public static void main(String[] args){
		Scanner entrada = new Scanner(System.in);
		int miFila=0		, miColumna=0;
		int personajeFila=0	, personajeColumna=0;
		int salir=0;
		String ordenUsuario;
		
		int[][] elMundo = {
			{0,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3},
			{0,1,2,2,2,2,2,2,2,0,1,1,0,0,0,0,0,1,0},
			{0,0,1,0,1,1,1,0,1,0,0,0,0,1,0,1,0,1,0},
			{0,0,1,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0},
			{1,0,0,0,1,0,1,1,1,1,1,0,0,1,1,1,0,1,0},
			{1,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0},
			{1,1,0,0,1,1,1,0,1,0,0,0,0,1,1,1,0,1,0},
			{0,1,1,0,0,0,0,0,2,2,2,1,0,1,0,0,0,0,0},
			{0,0,0,0,1,0,1,0,2,2,2,0,0,0,0,0,0,0,0},
			{0,1,0,0,0,0,0,0,0,2,2,2,2,1,1,1,0,1,0},
			{0,0,0,0,1,1,1,0,1,0,0,0,2,1,1,1,0,1,0},
			{0,1,1,0,0,0,0,0,1,0,1,1,0,0,0,0,0,1,0},
			{0,0,1,0,1,1,1,0,1,0,0,0,0,1,0,1,0,1,0},
			{0,0,1,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0},
			{0,0,1,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0},
			{1,0,0,0,1,0,1,1,1,1,1,0,0,1,1,1,0,1,0},
			{1,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0},
			{1,0,0,0,1,0,1,1,1,1,1,0,0,1,1,1,0,1,0},
			{0,0,0,0,1,0,1,0,1,0,1,0,0,0,0,0,0,0,0},
			{0,1,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,1,0},
			{0,0,0,0,1,1,1,0,1,0,0,0,0,1,1,1,0,1,0}
		};
		System.out.println("-------------------------------------------------------------------------");
		do {
			// INICIO IMPRESION DE MAPA (INCLUYE AL PERSONAJE) 
			for (miFila=0; miFila<elMundo.length; miFila=miFila+1){
				for (miColumna=0; miColumna<elMundo[miFila].length; miColumna=miColumna+1){
				
					if ((miFila==personajeFila) && (miColumna==personajeColumna)) {
						System.out.print("@@");
					} else {
						if (elMundo[miFila][miColumna]==0) {System.out.print("  ");}
						if (elMundo[miFila][miColumna]==1) {System.out.print("[]");}
						if (elMundo[miFila][miColumna]==2) {System.out.print("oO");}						
						if (elMundo[miFila][miColumna]==3) {System.out.print("/\\");}							
					}					
				}
				System.out.println();
			}
			// FIN DE IMPRESION DE MAPA (INCLUYE AL PERSONAJE) 
			System.out.println("-------------------------------------------------------------------------");
			System.out.println("Ingrese comando (w/s/d/a/Mirar/Salir)");
			ordenUsuario = entrada.nextLine();
			System.out.println("-------------------------------------------------------------------------");
			
			if (ordenUsuario.equalsIgnoreCase("w") && personajeFila>0 ) 										{ 
				if (elMundo[personajeFila-1][personajeColumna]!=1) {personajeFila = personajeFila-1;}				
			}
			if (ordenUsuario.equalsIgnoreCase("s")   && personajeFila<elMundo.length-1 ) 						{ 
				if (elMundo[personajeFila+1][personajeColumna]!=1) {personajeFila = personajeFila+1;}				
			}
			if (ordenUsuario.equalsIgnoreCase("d")  && personajeColumna < elMundo[personajeFila].length-1 )
				if (elMundo[personajeFila][personajeColumna+1]!=1) {personajeColumna = personajeColumna+1;}
			
			if (ordenUsuario.equalsIgnoreCase("a") && personajeColumna>0 )
				if (elMundo[personajeFila][personajeColumna-1]!=1) {personajeColumna = personajeColumna -1;}

			if (ordenUsuario.equalsIgnoreCase("q") && personajeColumna>0 && personajeFila>0 )
				if (elMundo[personajeFila-1][personajeColumna-1]!=1) {personajeFila = personajeFila-1; personajeColumna = personajeColumna -1;}
			
			if (ordenUsuario.equalsIgnoreCase("e") && personajeFila>0 && personajeColumna < elMundo[personajeFila].length-1)
				if (elMundo[personajeFila-1][personajeColumna+1]!=1) {personajeFila = personajeFila-1;   personajeColumna = personajeColumna+1;}
			
			if (ordenUsuario.equalsIgnoreCase("z") && personajeFila<elMundo.length-1 && personajeColumna>0)
				if (elMundo[personajeFila+1][personajeColumna-1]!=1) {personajeFila = personajeFila+1; personajeColumna = personajeColumna -1;}

			if (ordenUsuario.equalsIgnoreCase("c") && personajeFila<elMundo.length-1 && personajeColumna < elMundo[personajeFila].length-1)
				if (elMundo[personajeFila+1][personajeColumna+1]!=1) {personajeFila = personajeFila+1; personajeColumna = personajeColumna+1;}
			
			                              
			if (ordenUsuario.equalsIgnoreCase("salir")) 	{ salir=1;}
			
			// System.out.println("FILA="+personajeFila+" COLUMNA="+personajeColumna);
		} while (salir!=1);		
	}
}